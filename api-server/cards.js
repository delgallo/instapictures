﻿const clone = require('clone')

let db = {}

const defaultData = {
  "1xf0y6ziyjabvozdd253nd": {
    id: '1xf0y6ziyjabvozdd253nd',
    username: 'person01',
    userIconLink: 'person01.jpg',
    imageLink: 'cat1.jpeg',
    description: 'Description of cat 01.',
    likes: 53,
    liked: true,
    deleted: false,
    commentCount: 2
  },
  "2ni6ok3ym7mf1p33lnez": {
    id: '2ni6ok3ym7mf1p33lnez',
    username: 'person02',
    userIconLink: 'person02.jpg',
    imageLink: 'cat2.jpeg',
    description: 'Description of cat 02.',
    likes: 673,
    liked: false,
    deleted: false,
    commentCount: 0
  },
  "3ni6ok3ym7mf1p33lnez": {
    id: '3ni6ok3ym7mf1p33lnez',
    username: 'person03',
    userIconLink: 'person03.jpg',
    imageLink: 'cat3.jpeg',
    description: 'Description of cat 03.',
    likes: 292,
    liked: false,
    deleted: false,
    commentCount: 0
  },
  "4ni6ok3ym7mf1p33lnez": {
    id: '4ni6ok3ym7mf1p33lnez',
    username: 'person04',
    userIconLink: 'person04.jpg',
    imageLink: 'cat4.jpeg',
    description: 'Description of cat 04.',
    likes: 785,
    liked: true,
    deleted: false,
    commentCount: 0
  },
  "5ni6ok3ym7mf1p33lnez": {
    id: '5ni6ok3ym7mf1p33lnez',
    username: 'person05',
    userIconLink: 'person05.jpg',
    imageLink: 'cat5.jpeg',
    description: 'Description of cat 05.',
    likes: 497,
    liked: false,
    deleted: false,
    commentCount: 0
  }
}

function getData(token) {
  let data = db[token]
  if (data == null) {
    data = db[token] = clone(defaultData)
  }
  return data
}

function getAll(token) {
  return new Promise((res) => {
    const cards = getData(token)
    let keys = Object.keys(cards)
    let filtered_keys = keys.filter(key => !cards[key].deleted)
    res(filtered_keys.map(key => cards[key]))
  })
}

function like(token, id, option) {
  return new Promise((res) => {
    let cards = getData(token)
    card = cards[id]
    switch (option) {
      case "upLike":
        card.likes = card.likes + 1
        card.liked = true
        break
      case "downLike":
        card.likes = card.likes - 1
        card.liked = false
        break
      default:
        console.log(`cards.like received incorrect parameter: ${option}`)
    }
    res(card)
  })
}

function get(token, id) {
  return new Promise((res) => {
    const cards = getData(token)
    res(
      cards[id].deleted
        ? {}
        : cards[id]
    )
  })
}


function disable(token, id) {
  return new Promise((res) => {
    let cards = getData(token)
    cards[id].deleted = true
    res(cards[id])
  })
}

function incrementCommentCounter(token, id, count) {
  const data = getData(token)
  if (data[id]) {
    data[id].commentCount += count
  }
}

module.exports = {
  get,
  getAll,
  like,
  disable,
  incrementCommentCounter
}
