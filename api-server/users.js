const clone = require('clone')

let db = {}

const defaultData = {
  "axf0y6ziyjabvozdd253nd": {
    key: 'axf0y6ziyjabvozdd253nd',
    text: 'person01'
    ,value: 'person01'
  },
  "bni6ok3ym7mf1p33lnez": {
    key: 'bni6ok3ym7mf1p33lnez',
    text: 'person02'
    ,value:'person02'
  },
  "cni6ok3ym7mf1p33lnez": {
    key: 'cni6ok3ym7mf1p33lnez',
    text: 'person03'
    ,value:'person03'
  },
  "dni6ok3ym7mf1p33lnez": {
    key: 'dni6ok3ym7mf1p33lnez',
    text: 'person04'
    ,value: 'person04'
  },
  "eni6ok3ym7mf1p33lnez": {
    key: 'eni6ok3ym7mf1p33lnez',
    text: 'person05'
    ,value: 'person05'
  }
}

function getData (token) {
  let data = db[token]
  if (data == null) {
    data = db[token] = clone(defaultData)
  }
  return data
}

function getAll (token) {
  return new Promise((res) => {
    const users = getData(token)
    let keys = Object.keys(users)
    let filtered_keys = keys.filter(key => users[key].username!=='')
    res(filtered_keys.map(key => users[key]))
  })
}

module.exports = {  
  getAll
}
