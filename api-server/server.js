require('dotenv').config()

const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const config = require('./config')
const cards = require('./cards')
const comments = require('./comments')
const users = require('./users')
const app = express()

app.use(express.static('public'))
app.use(cors())


app.get('/', (req, res) => {
  const help = `
  <pre>
    Welcome to the MichelDelgallo Test MVF API!

    Use an Authorization header to work with your own data:

    fetch(url, { headers: { 'Authorization': 'whatever-you-want' }})

    The following endpoints are available:

    GET /cards
      USAGE:
        Get all of the cards. Useful for the main page when no category is selected.

    GET /cards/:id
        USAGE:
          Get card by id.

    POST /cards/:id
        USAGE:
          Used for like on a card
        PARAMS:
          option - String: Either "upLike" or "downLike"

    DELETE /cards/:id
        USAGE:
          Sets the deleted flag for a card to 'true'.

    GET /cards/:id/comments
        USAGE:
          Get all the comments for a single card

    POST /comments
        USAGE:
          Add a comment to a card

    GET /users
        USAGE:
          Get all users of the cards. 
    
 </pre>
  `
  res.send(help)
})

app.use((req, res, next) => {
  const token = req.get('Authorization')

  if (token) {
    req.token = token
    next()
  } else {
    res.status(403).send({
      error: 'Please provide an Authorization header to identify yourself (can be whatever you want)'
    })
  }
})

app.get('/cards', (req, res) => {
  cards.getAll(req.token)
    .then(
    (data) => res.send(data),
    (error) => {
      console.error(error)
      res.status(500).send({
        error: 'There was an error.'
      })
    }
    )
})

app.get('/cards/:id', (req, res) => {
  cards.get(req.token, req.params.id)
    .then(
    (data) => res.send(data),
    (error) => {
      console.error(error)
      res.status(500).send({
        error: 'There was an error.'
      })
    }
    )
})

app.post('/cards/:id', bodyParser.json(), (req, res) => {
  const { option } = req.body
  const id = req.params.id
  cards.like(req.token, id, option)
    .then(
    (data) => res.send(data),
    (error) => {
      console.error(error)
      res.status(500).send({
        error: 'There was an error.'
      })
    }
    )
})

app.delete('/cards/:id', (req, res) => {
  cards.disable(req.token, req.params.id)
    .then(
    (data) => res.send(data),
    (error) => {
      console.error(error)
      res.status(500).send({
        error: 'There was an error.'
      })
    }
    )
})

app.get('/cards/:id/comments', (req, res) => {
  comments.getByParent(req.token, req.params.id)
    .then(
    (data) => res.send(data),
    (error) => {
      console.error(error)
      res.status(500).send({
        error: 'There was an error.'
      })
    }
    )
})

app.post('/comments', bodyParser.json(), (req, res) => {
  comments.add(req.token, req.body)
    .then(
    (data) => res.send(data),
    (error) => {
      console.error(error)
      res.status(500).send({
        error: 'There was an error.'
      })
    }
    )
})

app.get('/users', (req, res) => {
  users.getAll(req.token)
    .then(
    (data) => res.send(data),
    (error) => {
      console.error(error)
      res.status(500).send({
        error: 'There was an error.'
      })
    }
    )
})


app.listen(config.port, () => {
  console.log('Server listening on port %s, Ctrl+C to stop', config.port)
})
