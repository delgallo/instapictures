﻿#InstaPictures example using React / React-Redux / React-Semantic-UI

##Contact
*Michel Legendre Delgallo
*E-mail: micheldelgallo@gmail.com
*Skype Name: mdelgallo
*Phone: +55 19988100101

##API - How to install and start the API Server:
* `cd api-server`
* `yarn install`
* `node server`

##Front-end - How to install and start the Front-end:
* `cd frontend`
* `yarn install`
* `yarn start`