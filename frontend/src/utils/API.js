const api = "http://localhost:3001";

let token = localStorage.token;
if (!token) {
    token = localStorage.token = Math.random().toString(36).substr(-8);
}

const headers = {
    'Accept': 'application/json',
    'Authorization': token
}

export const getAllCards = () =>
    fetch(`${api}/cards`, { headers })
        .then(res => res.json());

export const likeCard = (card_id, likeOption) =>
    fetch(`${api}/cards/${card_id}`, {
        method: 'POST',
        headers: {
            ...headers,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(likeOption)
    }).then(res => res.json());

export const getCardDetail = (Id) =>
    fetch(`${api}/cards/${Id}`, { headers })
        .then(res => res.json())

export const removeCard = (card_id) =>
    fetch(`${api}/cards/${card_id}`, {
        method: 'DELETE',
        headers
    }).then(res => res.json());

export const getAllCommentsByCardId = (card_id) =>
    fetch(`${api}/cards/${card_id}/comments`, { headers })
        .then(res => res.json());

export const saveComment = (CommentModel) =>
    fetch(`${api}/comments`, {
        method: 'POST',
        headers: {
            ...headers,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(CommentModel)
    }).then(res => res.json());

export const getAllUsers = () =>
    fetch(`${api}/users`, { headers })
        .then(res => res.json());










