import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import ListCards from './ListCards'

import {
    ListCardsAction
    , CardLikeAction
} from '../actions/CardsActions';

class Main extends Component {

    componentDidMount() {
        this.props.ListCardsAction();
    }


    onLike = (id, option) => {
        this.props.CardLikeAction(id, option);
    }

    constructor(props) {
        super(props);
    }

    render() {
        let { history, cards } = this.props;

        return (
            <div>
                <ListCards
                    cards={cards}
                    onLike={this.onLike}
                />
            </div >
        )

    }

}

const mapStateToProps = state => (
    {
        cards: state.CardsReducer.cards
    }
);

export default withRouter(connect(mapStateToProps, {
    ListCardsAction
    , CardLikeAction
})(Main));
