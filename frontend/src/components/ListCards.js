import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Button, Card, Image, Icon, Label } from 'semantic-ui-react'
import { Link } from 'react-router-dom'

class ListCards extends Component {

    render() {
        let { cards, onLike } = this.props

        return (
            <div>

                <Card.Group>
                    {cards.map((card) => (
                        <Card key={card.id}>
                            <Card.Content size="small">
                                <Image floated='left' size='huge' src={`/assets/avatars/${card.userIconLink}`} avatar />
                                <Card.Header>
                                    {card.username}
                                </Card.Header>

                                <Link to={`/cards/${card.id}`}>
                                    <Image src={`/assets/pictures/${card.imageLink}`} />
                                </Link>
                                <div className='ui two buttons'>

                                    <Button as='div' labelPosition='right'>
                                        <Button color={card.liked ? 'red' : 'grey'} onClick={(e) => onLike(card.id, (card.liked ? 'downLike' : 'upLike'))}>
                                            <Icon name='heart' />
                                            Like
                        </Button>
                                        <Label as='a' basic color={card.liked ? 'red' : 'grey'} pointing='left'>{card.likes}</Label>
                                    </Button>

                                    <Link to={`/cards/${card.id}`}>
                                        <Button>
                                            <Icon name='comment' />
                                            {card.commentCount}
                                        </Button>
                                    </Link>

                                </div>
                                <Card.Description>
                                    {card.description}
                                </Card.Description>
                            </Card.Content>


                        </Card>
                    ))}
                </Card.Group>


            </div>
        )
    }
}

ListCards.defaultProps = {
    cards: []
};

ListCards.propTypes = {
    cards: PropTypes.array.isRequired
};

export default ListCards










