import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {
    detailGetCardAction
    , detailGetAllCommentsByCardIdAction
    , detailLikeCardAction
    , detailDeleteCardAction
    , detailUsersAction
    //,detailHandleChangeCommentAction
    , detailCommentInsertAction
} from '../actions/DetailActions';


import {
    ListCardsAction
} from '../actions/CardsActions';


import CardItem from './CardItem'

class Detail extends Component {

    componentDidMount() {
        let { CardModel, comments, history, users } = this.props;
        let cardId = this.props.match.params.cardId;
        this.props.detailGetCardAction(cardId);
        this.props.detailUsersAction();
        this.props.detailGetAllCommentsByCardIdAction(cardId);

    }

    onLike = (id, option) => {
        this.props.detailLikeCardAction(id, option);
    }

    onDelete = (id, history) => {
        this.props.detailDeleteCardAction(id, history);
        this.props.ListCardsAction();
    }

    constructor(props) {
        super(props);
    }

    render() {
        let { CardModel, comments, history, users } = this.props;

        return (
            <div>
                <Link to="/">Back</Link>
                <CardItem
                    CardModel={CardModel}
                    comments={comments}
                    onLike={this.onLike}
                    onDelete={this.onDelete}
                    history={history}
                    users={users}
                    //handleChangeComment={this.props.detailHandleChangeCommentAction}
                    handleInsertComment={this.props.detailCommentInsertAction}
                />
            </div >
        )

    }

}

const mapStateToProps = state => (
    {
        CardModel: state.DetailReducer.CardModel
        , comments: state.DetailReducer.comments
        , users: state.DetailReducer.users
    }
);

export default withRouter(connect(mapStateToProps, {
    detailGetCardAction
    , detailGetAllCommentsByCardIdAction
    , detailLikeCardAction
    , detailDeleteCardAction
    , detailUsersAction
    , ListCardsAction
    //,detailHandleChangeCommentAction
    , detailCommentInsertAction
})(Detail));