import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Card, Image, Icon, Label, Comment, Header, Segment } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router';
import moment from 'moment';
import Avatar from 'react-avatar';
import { Form, Input, TextArea, Select } from 'formsy-semantic-ui-react'

class CardItem extends Component {

    onValidSubmit = (formData) => {
        this.props.handleInsertComment(formData.body, this.props.CardModel.id, formData.username)
        this.form.reset()
    };

    render() {
        let { CardModel, comments, onLike, onDelete, history, CommentModel, users } = this.props
        const errorLabel = <Label color="red" pointing />

        return (
            <div>

                <Card.Group>
                    <Card key={CardModel.id}>

                        <Card.Content size="large">
                            <Card.Header>
                                <div className="ui center aligned">
                                    {CardModel.username}
                                </div>
                            </Card.Header>
                            <div className="ui center aligned">
                                <Avatar src={`/assets/avatars/${CardModel.userIconLink}`} round={true} />
                            </div>
                            <Image src={`/assets/pictures/${CardModel.imageLink}`} />

                            <div className='ui four buttons'>
                                <Button as='div' labelPosition='right'>
                                    <Button color={CardModel.liked ? 'red' : 'grey'} onClick={(e) => onLike(CardModel.id, (CardModel.liked ? 'downLike' : 'upLike'))}>
                                        <Icon name='heart' />
                                        Like
                                </Button>
                                    <Label as='a' basic color={CardModel.liked ? 'red' : 'grey'} pointing='left'>{CardModel.likes}</Label>
                                </Button>

                                <Button>
                                    <Icon name='comment' />
                                    {CardModel.commentCount}
                                </Button>


                            </div>
                            <Card.Description>
                                {CardModel.description}
                            </Card.Description>
                            <Button icon='remove' onClick={(e) => onDelete(CardModel.id, this.props.history)} />
                        </Card.Content>
                    </Card>
                </Card.Group>


                <Comment.Group size='mini'>

                    <Segment inverted>
                        <Form reply inverted name="FormCommentInsert"
                            ref={ref => this.form = ref}
                            onValidSubmit={this.onValidSubmit}
                        >

                            <Select label="Username"
                                name="username"
                                options={users.filter(u => u.key != 0)}
                                selection
                                placeholder='Username'
                                required
                                validationErrors={{
                                    isDefaultRequiredValue: 'Select the username',
                                }}
                                errorLabel={errorLabel}
                            />

                            <Form.TextArea placeholder='Fill the content...'
                                onChange={this.props.handleChangeComment}
                                name="body"
                                required={true}
                                required
                                label='Content'
                                validationErrors={
                                    {
                                        isDefaultRequiredValue: 'Fill the content'
                                    }
                                }
                                errorLabel={errorLabel}
                            />
                            <Button content='Comment' labelPosition='left' icon='edit' primary />

                        </Form>
                    </Segment>


                    <Header as='h3' dividing>Comments</Header>
                    {comments.map((comment) => (
                        <Comment key={comment.id}>
                            <Comment.Content>
                                <Comment.Author as='a'>{comment.username}</Comment.Author>
                                <Comment.Metadata>
                                    <span>{moment(comment.timestamp).format("DD/MM/YY HH:mm")}</span>
                                </Comment.Metadata>
                                <Comment.Text>{comment.body}</Comment.Text>
                            </Comment.Content>
                        </Comment>
                    ))}

                </Comment.Group>
            </div>
        )
    }
}

CardItem.propTypes = {
    comments: PropTypes.array.isRequired
    , onLike: PropTypes.func.isRequired
    , onDelete: PropTypes.func.isRequired
    , users: PropTypes.array.isRequired
    //, handleChangeComment: PropTypes.func.isRequired
    , handleInsertComment: PropTypes.func.isRequired
};

export default CardItem










