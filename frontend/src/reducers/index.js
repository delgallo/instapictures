import { combineReducers } from 'redux';
import CardsReducer from './CardsReducer';
import DetailReducer from './DetailReducer';

export default combineReducers({
    CardsReducer
    , DetailReducer
})

