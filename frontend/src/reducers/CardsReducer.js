import {
    LIST_CARDS
    , LIKE_CARDS
    , REMOVE_CARD
} from '../actions/ActionsTypes';

const INITIAL_STATE = {
    cards: []
    , users: []
}

export default (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case LIST_CARDS:
            return { ...state, cards: action.payload }
        case LIKE_CARDS:
            return { ...state, cards: state.cards.map(c => (c.id === action.payload.id ? action.payload : c)) }
        case REMOVE_CARD:
            return { ...state, cards: [...state.cards.filter(c => action.payload.id !== c.id)] }
        default:
            return state;
    }
}