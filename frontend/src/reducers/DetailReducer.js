import {
    DETAIL_GET_CARD
    , DETAIL_GET_ALL_COMMENTS
    , DETAIL_LIKE_CARD
    , DETAIL_DELETE_CARD
    , DETAIL_LIST_USERS
    , DETAIL_COMMENT_HANDLE_CHANGE
    , DETAIL_COMMENT_INSERT
} from '../actions/ActionsTypes';


import { CardModel, CommentModel } from '../models/';

const INITIAL_STATE = {
    CardModel
    , comments: []
    //, CommentModel
    , users: []
}

export default (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case DETAIL_GET_CARD:
            return { ...state, CardModel: { ...action.payload } }
        case DETAIL_GET_ALL_COMMENTS:
            return { ...state, comments: [...action.payload] }
        case DETAIL_LIKE_CARD:
            return { ...state, CardModel: action.payload }
        case DETAIL_DELETE_CARD:
            return { ...state, CardModel: { CardModel }, comments: [] }
        case DETAIL_LIST_USERS:
            return { ...state, users: action.payload }
        //case DETAIL_COMMENT_HANDLE_CHANGE:
        //return { ...state, CommentModel: { ...state.CommentModel, [action.field]: action.payload } } 
        case DETAIL_COMMENT_INSERT:
            return { ...state, comments: [...state.comments, action.payload] }
        default:
            return state;
    }
}