import React, { Component } from 'react';
import { Route } from 'react-router-dom'
import Main from './components/Main.js'
import Detail from './components/Detail.js'
import { Container, Header } from 'semantic-ui-react'
import { connect } from 'react-redux'

class App extends Component {
  render() {
    return (
      <div>
        <Route exact path="/" component={Main} />
        <Route path="/cards/:cardId" component={Detail} />
      </div>
    );
  }
}

export default App;
