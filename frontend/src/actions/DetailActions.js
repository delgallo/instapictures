import {
    DETAIL_GET_CARD
    , DETAIL_GET_ALL_COMMENTS
    , DETAIL_LIKE_CARD
    , DETAIL_DELETE_CARD
    , DETAIL_LIST_USERS
    , DETAIL_COMMENT_HANDLE_CHANGE
    , DETAIL_COMMENT_INSERT
    , REMOVE_CARD
    , LIKE_CARDS
} from './ActionsTypes';

import uuid from 'uuid';
import moment from 'moment';

import { CommentModel } from '../models/';

import * as Api from '../utils/API';

export const detailGetCardAction = (id) => {
    return dispatch => {
        Api.getCardDetail(id).then(card => {
            dispatch({ type: DETAIL_GET_CARD, payload: card });
        });
    }
}

export const detailGetAllCommentsByCardIdAction = (id) => {
    return dispatch => {
        Api.getAllCommentsByCardId(id).then(comments => {
            dispatch({ type: DETAIL_GET_ALL_COMMENTS, payload: comments });
        });
    }
}

export const detailLikeCardAction = (card_id, option) => {
    return dispatch => {
        Api.likeCard(card_id, { option }).then(card => {
            dispatch({ type: DETAIL_LIKE_CARD, payload: card });
            dispatch({ type: LIKE_CARDS, payload: card });

        });
    }
}

export const detailDeleteCardAction = (card_id, history) => {
    return dispatch => {
        Api.removeCard(card_id).then(card => {
            dispatch({ type: DETAIL_DELETE_CARD, payload: card })
            dispatch({ type: REMOVE_CARD, payload: card })
            history.push("/");
        });
    }
}

export const detailUsersAction = () => {
    return dispatch => {
        Api.getAllUsers().then(users => {
            users.map(user => {
                dispatch({ type: DETAIL_LIST_USERS, payload: users });
            });
        });
    }
}

export const detailHandleChangeCommentAction = (event) => {
    return {
        type: DETAIL_COMMENT_HANDLE_CHANGE
        , field: event.target.name
        , payload: event.target.value
    }
}

export const detailCommentInsertAction = (body, cardId, user) => {
    let newComment = {...CommentModel};
    newComment.id = uuid.v1();
    newComment.timestamp = moment().valueOf();
    newComment.parentId = cardId;
    newComment.username = user;
    newComment.body = body;

    return dispatch => {
        Api.saveComment(newComment).then(comment => {
            dispatch({ type: DETAIL_COMMENT_INSERT, payload: comment });
            dispatch(detailGetAllCommentsByCardIdAction(cardId));
            dispatch(detailGetCardAction(cardId));
        });
    }
}

