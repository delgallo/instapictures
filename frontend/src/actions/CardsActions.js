import {
    LIST_CARDS
    , LIKE_CARDS
    , DETAIL_DELETE_CARD
} from './ActionsTypes';

import * as Api from '../utils/API';


export const ListCardsAction = () => {
    return dispatch => {
        Api.getAllCards().then(cards => {
            cards.map(card => {
                dispatch({ type: LIST_CARDS, payload: cards });
                dispatch({ type: DETAIL_DELETE_CARD, payload: {} });

            });
        });
    }
}

export const CardLikeAction = (card_id, option) => {
    return dispatch => {
        Api.likeCard(card_id, { option }).then(card => {
            dispatch({ type: LIKE_CARDS, payload: { ...card } });
        });
    }
}


