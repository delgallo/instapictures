export const LIST_CARDS = "LIST_CARDS";
export const LIKE_CARDS = "LIKE_CARDS";
export const REMOVE_CARD = "REMOVE_CARD";

export const DETAIL_GET_CARD = "DETAIL_GET_CARD";
export const DETAIL_GET_ALL_COMMENTS = "DETAIL_GET_ALL_COMMENTS";
export const DETAIL_LIKE_CARD = "DETAIL_LIKE_CARD";
export const DETAIL_DELETE_CARD = "DETAIL_DELETE_CARD";
export const DETAIL_LIST_USERS = "DETAIL_LIST_USERS";
export const DETAIL_COMMENT_HANDLE_CHANGE = "DETAIL_COMMENT_HANDLE_CHANGE";
export const DETAIL_COMMENT_INSERT = "DETAIL_COMMENT_INSERT";