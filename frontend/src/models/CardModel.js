export const CardModel = {
    id: '',
    username: '',
    userIconLink: '',
    imageLink: '',
    description: '',
    likes: 0,
    liked: false,
    deleted: false,
    commentCount: 0
}